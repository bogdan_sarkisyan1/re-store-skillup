import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import {connect} from "react-redux";

class Header extends Component {
  render() {

    const {
      goodsInCart,
      totalPrice,
    } = this.props;

    return (
      <div className='header'>
        <div className="logo">
          Re-Store
        </div>
        <div className="nav">
          <div className="cart">
            <Link to="/cart" className='cart__link'>
              <i className="fas fa-shopping-cart"></i>
              <span>Total: {totalPrice}$</span>
              <span>Goods: {goodsInCart}</span>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    goodsInCart: state.cart.goods,
    totalPrice: state.cart.orderTotal,
  }
};

export default connect(mapStateToProps)(Header);