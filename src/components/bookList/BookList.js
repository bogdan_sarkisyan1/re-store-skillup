import React, {Component} from 'react';
import BookListItem from "../bookListItem";
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { withBookstoreService } from '../hoc';
import { compose } from '../../utils';
import Loader from '../loader'
import ErrorIndicator from "../errorIndicator";
import bookstoreService from "../../services/bookstoreService";
import {bindActionCreators} from "redux";

const BookList = ({books, onAddedToCart}) => {
  return (
    <ul className='book-list'>
      {
        books.map((book) => {
          return <li  className='book-list__item' key={book.id}><BookListItem onAddedToCart={() => onAddedToCart(book.id)}  book={book}/></li>
        })
      }
    </ul>
  );
};

class BookListContainer extends Component {

  componentDidMount() {
    this.props.fetchBooks();
  }

  render() {
    const { books, loading, error, onAddedToCart } = this.props;

    if (error) {
      console.log(error);
      return <ErrorIndicator/>
    }
    if (loading) {
      return <Loader/>
    }

    return <BookList onAddedToCart={onAddedToCart} books={books}/>
  }
}



const mapStateToProps = (state) => {
  return {
    books: state.bookList.books,
    loading: state.bookList.loading,
    error: state.bookList.error
  }
};

const mapDispatchToProps = (dispatch, {bookstoreService}) => {
  return bindActionCreators({
    fetchBooks: actions.fetchBooks(bookstoreService),
    onAddedToCart: actions.dispatchAfterOrderChange(actions.bookAddedToCart),
  }, dispatch)
};

export default compose(
  withBookstoreService(),
  connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);
