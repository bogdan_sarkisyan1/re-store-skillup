import React, {Component} from 'react';
import BookList from '../bookList';
import Header from '../header';
import CartTable from "../cartTable";

class HomePage extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <Header/>
          <BookList/>
          <CartTable/>
        </div>
      </div>
    );
  }
}

export default HomePage;