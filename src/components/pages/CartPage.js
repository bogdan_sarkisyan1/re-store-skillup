import React, {Component} from 'react';
import CartTable from "../cartTable";
import { Link } from 'react-router-dom';

class CartPage extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <CartTable/>
          <Link to='/'>Back to store</Link>
        </div>
      </div>
    );
  }
}

export default CartPage;