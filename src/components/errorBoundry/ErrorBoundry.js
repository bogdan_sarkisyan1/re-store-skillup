import React, {Component} from 'react';
import ErrorIndicator from "../errorIndicator";

class ErrorBoundry extends Component {

  constructor() {
    super();

    this.state = {
      hasError: false,
    }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({hasError: true})
  }

  render() {

    if (this.state.hasError) {
      return <ErrorIndicator/>
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundry;