import React, {Component, Fragment} from 'react';

class BookListItem extends Component {
  render() {

    const {
      book: {
        title,
        author,
        price,
        coverImage,
      },
      onAddedToCart
    } = this.props;

    return (
      <Fragment>
        <div className="book-list__item-image-cover">
          <img src={coverImage} alt="book cover"/>
        </div>
        <div className="book-list__item-info">
          <div className="book-list__item-info-row">
            <span className="book-list__item-info-title">Title:</span>
            <span className="book-list__item-title">{title}</span>
          </div>
          <div className="book-list__item-info-row">
            <span className="book-list__item-info-title">Author:</span>
            <span className="book-list__item-author">{author}</span>
          </div>
          <div className="book-list__item-info-row">
            <span className="book-list__item-info-title">Price:</span>
            <span className="book-list__item-title">{price}$</span>
          </div>
          <button
            className="btn"
            onClick={onAddedToCart}
          >Add to cart</button>
        </div>

      </Fragment>
    );
  }
}

export default BookListItem;