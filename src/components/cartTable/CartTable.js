import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import {bindActionCreators} from "redux";

class CartTable extends Component {
  render() {

    const {
      items,
      orderTotal,
      onIncrease,
      onDecrease,
      onDelete,
      onDeleteAll
    } = this.props;

    const CartTableRow = (item, idx) => {
      const { id, title, amount, totalPrice} = item;
      return (
        <tr key={id}>
          <td>{idx + 1}</td>
          <td>{title}</td>
          <td>{amount}</td>
          <td>{totalPrice}$</td>
          <td>
            <button
              onClick={() => onIncrease(id)}
              className="btn"
            >
              <i className="fas fas--sm fa-plus"></i>
            </button>
            <button
              className="btn"
              onClick={() => onDecrease(id)}
            >
              <i className="fas fas--sm fa-minus"></i>
            </button>
            <button
              className="btn"
              onClick={() => onDelete(id)}
            >
              <i className="fas fas--sm fa-trash"></i>
            </button>
          </td>
        </tr>
      )
    };

    return (
      <div className='cart-table'>
        <h2>Your order</h2>
        <table className="table">
          <thead>
            <tr>
              <th>№</th>
              <th>Item</th>
              <th>Count</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          {
            items.map((item, idx) => {
              return CartTableRow(item, idx)
            })
          }
          </tbody>
        </table>
        <button className="btn" onClick={onDeleteAll}>Delete all</button>
        <div className="total-price">
          Total: <span className="total-price__value">{orderTotal}$</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({cart:{cartItems, totalPrice, orderTotal}}, ownProps) => {
  return {
    items: cartItems,
    totalPrice,
    orderTotal
  }
};

const mapDispatchToProps = (dispatch) => {
  // return bindActionCreators({
  //   onIncrease: actions.dispatchAfterOrderChange(actions.bookAddedToCart),
  //   onDecrease: actions.dispatchAfterOrderChange(actions.bookRemovedFromCart),
  //   onDelete: actions.dispatchAfterOrderChange(actions.bookAmountRemovedFromCart),
  // }, dispatch)

  return {
    onIncrease: (id) => dispatch(actions.dispatchAfterOrderChange(actions.bookAddedToCart)(id)),
    onDecrease: (id) => dispatch(actions.dispatchAfterOrderChange(actions.bookRemovedFromCart)(id)),
    onDelete: (id) => dispatch(actions.dispatchAfterOrderChange(actions.bookAmountRemovedFromCart)(id)),
    onDeleteAll: () => dispatch(actions.dispatchAfterOrderChange(actions.booksDeletedAll)()),
  }
};

export default connect(mapStateToProps ,mapDispatchToProps)(CartTable);