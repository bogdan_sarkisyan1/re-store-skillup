import React, {Component, Fragment} from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import {
  HomePage,
  CartPage
} from '../pages';

class App extends Component {
  render() {
    return (
      <Switch>
        <Fragment>
          <Route
            exact
            path='/'
            render={() => {
              return (<HomePage/>)
            }}
          />
          <Route
            exact
            path='/cart'
            render={() => {
              return (<CartPage/>)
            }}
          />
        </Fragment>
      </Switch>
    );
  }
}

export default App;