import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from "react-router-dom";
import App from './components/app';
import ErrorBoudry from './components/errorBoundry';
import BookstoreService from './services/bookstoreService';
import { BookstoreServiceProvider } from './components/bookstoreServiceContext';
import store from './store';

const bookstoreService = new BookstoreService();

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoudry>
      <BookstoreServiceProvider value={bookstoreService}>
        <Router>
          <App/>
        </Router>
      </BookstoreServiceProvider>
    </ErrorBoudry>
  </Provider>
, document.getElementById('root'));