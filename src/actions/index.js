const booksLoaded = (newBooks) => {
  return {
    type: 'FETCH_BOOKS_SUCCESS',
    payload: newBooks
  }
};

const booksRequested = () => {
  return {
    type: 'FETCH_BOOKS_REQUEST'
  }
};

const booksError = (error) => {
  return {
    type: 'FETCH_BOOKS_FAIL',
    payload: error,
  }
};

const bookAddedToCart = (bookId) => {
  return {
    type: 'BOOK_ADDED_TO_CART',
    payload: bookId
  }
};

const bookRemovedFromCart = (bookId) => {
  return {
    type: 'BOOK_REMOVED_FROM_CART',
    payload: bookId,
  }
};

const bookAmountRemovedFromCart = (bookId) => {
  return {
    type: 'BOOK_AMOUNT_REMOVED_FROM_CART',
    payload: bookId,
  }
};

const booksDeletedAll = () => {
  return {
    type: 'BOOKS_DELETED_ALL'
  }
};

const dispatchAfterOrderChange = (creator) => (payload) => (dispatch) => {
  dispatch(creator(payload));
  dispatch(cartOrderDetailsUpdate());
};

const fetchBooks = (bookstoreService) => () => (dispatch) => {
  dispatch(booksRequested());
  bookstoreService.getBooks()
    .then((data) => dispatch(booksLoaded(data)))
    .catch((error) => dispatch(booksError(error)))
};

const cartOrderDetailsUpdate = () => {
  return {
    type: 'CART_ORDER_DETAILS_UPDATE',
  }
};

export {
  fetchBooks,
  bookAddedToCart,
  bookRemovedFromCart,
  bookAmountRemovedFromCart,
  dispatchAfterOrderChange,
  booksDeletedAll,
}