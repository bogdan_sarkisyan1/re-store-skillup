export default class BookstoreService {

  constructor() {
    this.errorThrow = false;
    this.data = [
      {
        id: 1,
        title: 'Harry Potter',
        author: 'Rowling Joanne',
        price: 30,
        coverImage: 'https://hpmedia.bloomsbury.com/rep/s/9781408855898_309038.jpeg'
      },
      {
        id: 2,
        title: 'The Witcher',
        author: 'Andrzej Sapkowski',
        price: 20,
        coverImage: 'https://upload.wikimedia.org/wikipedia/en/thumb/1/14/Andrzej_Sapkowski_-_The_Last_Wish.jpg/220px-Andrzej_Sapkowski_-_The_Last_Wish.jpg'
      }
    ];
  }

  getBooks() {
    return new Promise((res, rej) => {
      setTimeout(() => {
        if (this.errorThrow) {
          rej('testing errors')
        } else {
          res(this.data)
        }
      }, 700)
    })
  }
}