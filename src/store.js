import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './reducers';
import thunkMiddleware from 'redux-thunk';
import { loadState, saveState } from './utils/localStorage';
const persistedData = loadState();

const store = createStore(reducer, persistedData, compose(
  applyMiddleware(
    thunkMiddleware,
  ),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));
store.subscribe(() => {
  saveState({
    ...store.getState(),
    cart: store.getState().cart,
  });
});

export default store;