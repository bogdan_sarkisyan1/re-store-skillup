
import updateBookList from './bookList';
import updateCart from './cart';


const reducer = (state, action) => {
  
  return {
    cart: updateCart(state, action),
    bookList: updateBookList(state, action),
  }
};

export default reducer;