const updateCartItems = (cartItems, item ,itemIndex) => {

  if (item.amount <= 0) {
    return [
      ...cartItems.slice(0, itemIndex),
      ...cartItems.slice(itemIndex + 1)
    ]
  }

  if (itemIndex === -1) {
    return [
      ...cartItems,
      item
    ]
  } else {
    return [
      ...cartItems.slice(0, itemIndex),
      item,
      ...cartItems.slice(itemIndex + 1)
    ]
  }
};
const updateCartItem = (book, item = {}, quantity) => {

  const { id = book.id, title = book.title, amount = 0, totalPrice = 0 } = item;

  const newItem ={
    id,
    title,
    amount: amount + quantity,
    totalPrice: totalPrice + (book.price * quantity),
  };

  return newItem;
};
const updateOrder = (state, bookId, quantity) => {

  const { cart: { cartItems }, bookList: { books } } = state;

  const book = books.find((book) => book.id === bookId);
  const cartItemIndex = cartItems.findIndex((item) => book.id === item.id);
  const cartItem = cartItems[cartItemIndex];

  if (quantity === 0) {
    quantity = -cartItem.amount;
  }

  return updateCartItems(cartItems, updateCartItem(book, cartItem, quantity), cartItemIndex)
};

const orderTotalCalculate = (cartItemsUpdated) => {

  if (!cartItemsUpdated) return 0;

  let orderTotalPrice = 0;

  parseFloat(cartItemsUpdated.map((item) => orderTotalPrice += parseFloat(item.totalPrice)));

  return orderTotalPrice;
};

const updateCart = (state, action) => {

  if (!state) {
    return {
      cartItems: [],
      orderTotal: 0,
      goods: 0,
    }
  }

  switch(action.type) {
    case 'BOOK_ADDED_TO_CART':
      const newCartItemsAdded = updateOrder(state, action.payload, 1);
      return {
        ...state.cart,
        cartItems: newCartItemsAdded,
      };
    case "BOOK_REMOVED_FROM_CART":
      const newCartItemsRemoved = updateOrder(state, action.payload, -1);
      return {
        ...state.cart,
        cartItems: newCartItemsRemoved
      };

    case 'BOOK_AMOUNT_REMOVED_FROM_CART':
      const newCartItemsAmountRemoved = updateOrder(state, action.payload, 0);
      return {
        ...state.cart,
        cartItems: newCartItemsAmountRemoved
      };

    case 'BOOKS_DELETED_ALL':
      return {
        ...state.cart,
        cartItems: [],
      };

    case 'CART_ORDER_DETAILS_UPDATE':
      return {
        ...state.cart,
        orderTotal: orderTotalCalculate(state.cart.cartItems),
        goods: state.cart.cartItems.length,
      };

    default: return state.cart;
  }
};

export default updateCart;
